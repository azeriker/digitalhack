﻿using System;
using DigitalHack.Domain.Contracts;
using DigitalHack.Domain.Entities;

using System.Collections.Generic;
using System.Linq;
using DigitalHack.Domain.BaseEntities;
using DigitalHack.Domain.Entities.Enums;
using MongoDB.Bson;

namespace DigitalHack.Domain.Services
{
    public class InitiativeService : IInitiativeService
    {
        private readonly IInitiativeRepository _initiativeRepository;
        private readonly ICommentRepository _commentRepository;
        private readonly ReactionStrategy _reactionStrategy;

        public InitiativeService(
            IInitiativeRepository initiativeRepository,
            ICommentRepository commentRepository,
            ReactionStrategy reactionStrategy)
        {
            _initiativeRepository = initiativeRepository;
            _commentRepository = commentRepository;
            _reactionStrategy = reactionStrategy;
        }

        public void Like(Like like)
        {
            var initiative = _initiativeRepository.Get(like.EntityId);
            _reactionStrategy.Like(initiative, like);
            _initiativeRepository.Update(initiative);
        }

        public void Dislike(Dislike dislike)
        {
            var initiative = _initiativeRepository.Get(dislike.EntityId);
            _reactionStrategy.Dislike(initiative, dislike);
            _initiativeRepository.Update(initiative);
        }

        public void UnLike(Like like)
        {
            var initiative = _initiativeRepository.Get(like.EntityId);
            _reactionStrategy.UnLike(initiative, like);
            _initiativeRepository.Update(initiative);
        }

        public void UnDislike(Dislike dislike)
        {
            var initiative = _initiativeRepository.Get(dislike.EntityId);
            _reactionStrategy.UnDislike(initiative, dislike);
            _initiativeRepository.Update(initiative);
        }

        public void AddComment(string initiativeId, Comment comment)
        {
            comment.Date = DateTime.Now;
            comment.Id = ObjectId.GenerateNewId().ToString();

            _commentRepository.Add(comment);
            _initiativeRepository.AddComment(initiativeId, comment);
        }

        public string Add(Initiative initiative)
        {
            initiative.Date = DateTime.Now;
            initiative.Id = ObjectId.GenerateNewId().ToString();

            _initiativeRepository.Add(initiative);
            return initiative.Id;
        }

        public IEnumerable<Initiative> Get()
        {
            var a = _initiativeRepository.Get().ToList();
            a.Sort((x, y) => x.Rating - y.Rating);
            return a;
        }

        public Initiative Get(string id)
        {
            var initiative = _initiativeRepository.Get(id);
            return IncludeComments(initiative);
        }

        public IEnumerable<Initiative> GetByDistrict(string districtId)
        {
            var a = _initiativeRepository.Get(i => i.DistrictId, districtId).ToList();
            a.Sort((x, y) => x.Rating - y.Rating);
            return a;
        }

        public void SetManagerComment(string initiativeId, string comment)
        {
            var initiative = _initiativeRepository.Get(initiativeId);
            initiative.ManagerComment = comment;
            _initiativeRepository.Update(initiative);
        }

        public void ChangeStatus(string initiativeId, InitiativeStatus status)
        {
            var initiative = _initiativeRepository.Get(initiativeId);
            initiative.Status = status;
            _initiativeRepository.Update(initiative);
        }

        private Initiative IncludeComments(Initiative initiative)
        {
            var initiativeComments = _commentRepository.Get(initiative.CommentIds).ToList();
            var childComments = _commentRepository.Get(initiativeComments.SelectMany(i => i.CommentIds)).ToList();

            foreach (var rootComment in initiativeComments)
                rootComment.Comments = childComments.Where(i => rootComment.CommentIds.Contains(i.Id)).ToList();

            initiative.Comments = initiativeComments;
            return initiative;
        }
    }
}
