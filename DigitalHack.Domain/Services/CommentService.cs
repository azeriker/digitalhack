﻿using System;
using System.Collections.Generic;
using System.Linq;
using DigitalHack.Domain.BaseEntities;
using DigitalHack.Domain.Contracts;
using DigitalHack.Domain.Entities;
using MongoDB.Bson;

namespace DigitalHack.Domain.Services
{
    public class CommentService : ICommentService
    {
        private readonly ICommentRepository _commentRepository;
        private readonly ReactionStrategy _reactionStrategy;

        public CommentService(
            ICommentRepository commentRepository,
            ReactionStrategy reactionStrategy)
        {
            _commentRepository = commentRepository;
            _reactionStrategy = reactionStrategy;
        }

        public void Like(Like like)
        {
            var comment = _commentRepository.Get(like.EntityId);
            _reactionStrategy.Like(comment, like);
            _commentRepository.Update(comment);
        }

        public void Dislike(Dislike dislike)
        {
            var comment = _commentRepository.Get(dislike.EntityId);
            _reactionStrategy.Dislike(comment, dislike);
            _commentRepository.Update(comment);
        }

        public void UnLike(Like like)
        {
            var comment = _commentRepository.Get(like.EntityId);
            _reactionStrategy.UnLike(comment, like);
            _commentRepository.Update(comment);
        }

        public void UnDislike(Dislike dislike)
        {
            var comment = _commentRepository.Get(dislike.EntityId);
            _reactionStrategy.UnDislike(comment, dislike);
            _commentRepository.Update(comment);
        }

        public void Ban(IEnumerable<string> ids)
        {
            _commentRepository.MarkAsBanned(ids);
        }

        public void Validate(IEnumerable<string> ids)
        {
            _commentRepository.MarkAsValidated(ids);
        }

        public void Add(Comment comment)
        {
            _commentRepository.Add(comment);
        }

        public void AddComment(string parentCommentId, Comment comment)
        {
            comment.Date = DateTime.Now;
            comment.Id = ObjectId.GenerateNewId().ToString();

            _commentRepository.Add(comment);
            _commentRepository.AddChild(parentCommentId, comment);
        }

        public IEnumerable<Comment> Get(IEnumerable<string> ids)
        {
            return _commentRepository.Get(ids);
        }

        public Comment Get(string id)
        {
            return _commentRepository.Get(id);
        }

        public void Update(Comment comment)
        {
            _commentRepository.Update(comment);
        }
    }
}
