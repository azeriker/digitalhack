﻿using System.Collections.Generic;
using DigitalHack.Domain.Contracts;
using DigitalHack.Domain.Entities;

namespace DigitalHack.Domain.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public void Register(User user)
        {
            _userRepository.Add(user);
        }

        public User Get(int id)
        {
            return _userRepository.Get(id);
        }

        public User GetBySnils(string snils)
        {
            return _userRepository.Get(i => i.Snils, snils);
        }

    }
}
