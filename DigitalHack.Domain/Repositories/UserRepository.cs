﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using DigitalHack.Domain.Contracts;
using DigitalHack.Domain.Entities;
using MongoDB.Driver;

namespace DigitalHack.Domain.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly IMongoCollection<User> _collection;

        public UserRepository(MongoContext context)
        {
            _collection = context.Users;
        }

        public void Add(User user)
        {
            _collection.InsertOne(user);
        }

        public User Get(int id)
        {
            return _collection.Find(i => i.Id == id).FirstOrDefault();
        }

        public IEnumerable<User> Get()
        {
            return _collection.Find(_ => true).ToList();
        }

        public User Get<TField>(Expression<Func<User, TField>> field, TField value)
        {
            FilterDefinition<User> filter = Builders<User>.Filter.Eq(field, value);
            return _collection.Find(filter).FirstOrDefault();
        }

        public bool Exists(string snils)
        {
            return _collection.Find(i => i.Snils == snils).FirstOrDefault() != null;
        }

        public void Update(User user)
        {
            var filter = Builders<User>.Filter.Eq(i => i.Id, user.Id);
            var update = Builders<User>.Update
                .Set(i => i.Rating, user.Rating);

            _collection.UpdateOne(filter, update);
        }
    }
}
