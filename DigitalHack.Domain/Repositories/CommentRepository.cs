﻿using DigitalHack.Domain.Contracts;
using DigitalHack.Domain.Entities;
using DigitalHack.Domain.Models;

using MongoDB.Driver;

using System.Collections.Generic;

namespace DigitalHack.Domain.Repositories
{
    public class CommentRepository : BaseRepository<Comment>, ICommentRepository
    {
        public CommentRepository(MongoContext context) 
            : base(context, $"{nameof(Comment)}s")
        {
        }

        public void AddChild(string parentCommentId, Comment comment)
        {
            var filter = Builders<Comment>.Filter.Eq(i => i.Id, parentCommentId);
            var update = Builders<Comment>.Update
                .Push(i => i.CommentIds, comment.Id);

            Collection.UpdateOne(filter, update);
        }

        public IEnumerable<Comment> GetNotValidated()
        {
            var filter = Builders<Comment>.Filter.Eq(i => i.IsValidated, false);
            return Collection.Find(filter).ToList();
        }

        public void MarkAsBanned(IEnumerable<string> ids)
        {
            var filter = Builders<Comment>.Filter.In(i => i.Id, ids);
            var update = Builders<Comment>.Update.Set(i => i.IsBanned, true);

            Collection.UpdateMany(filter, update);
        }

        public void MarkAsValidated(IEnumerable<string> ids)
        {
            var filter = Builders<Comment>.Filter.In(i => i.Id, ids);
            var update = Builders<Comment>.Update.Set(i => i.IsValidated, true);

            Collection.UpdateMany(filter, update);
        }

        public void UpdateTonality(CommentTonalityBatchResultModel result)
        {
            var updateModels = new List<UpdateOneModel<Comment>>();
            foreach (var tonalityResult in result.Result)
            {
                var filter = Builders<Comment>.Filter.Eq(i => i.Id, tonalityResult.Id);
                var update = Builders<Comment>.Update.Set(i => i.Tonality, tonalityResult.Tonality);

                var updateModel = new UpdateOneModel<Comment>(filter, update);
                updateModels.Add(updateModel);
            }

            Collection.BulkWrite(updateModels);
        }

        public void Update(Comment comment)
        {
            var filter = Builders<Comment>.Filter.Eq(i => i.Id, comment.Id);
            var update = Builders<Comment>.Update
                .Set(i => i.CommentIds, comment.CommentIds)
                .Set(i => i.Likes, comment.Likes)
                .Set(i => i.Dislikes, comment.Dislikes);

            Collection.UpdateOne(filter, update);
        }
    }
}
