﻿using System;
using System.Collections.Generic;
using System.Text;
using DigitalHack.Domain.Contracts;
using DigitalHack.Domain.Entities;

namespace DigitalHack.Domain.Repositories
{
    public class RegionRepository : BaseRepository<Region>, IRegionRepository
    {
        public RegionRepository(MongoContext context) : base(context, $"{nameof(Region)}s")
        {
        }
    }
}
