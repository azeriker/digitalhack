﻿using System.Collections.Generic;
using DigitalHack.Domain.Contracts;
using DigitalHack.Domain.Entities;
using MongoDB.Driver;

namespace DigitalHack.Domain.Repositories
{
    public class DistrictRepository : BaseRepository<District>, IDistrictRepository
    {
        public DistrictRepository(MongoContext context) 
            : base(context, $"{nameof(District)}s")
        {
        }

        public IEnumerable<District> GetByManagerId(string managerId)
        {
            return Get(d => d.ManagerId, managerId);
        }
    }
}
