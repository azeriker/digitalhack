﻿using DigitalHack.Domain.Entities;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;

namespace DigitalHack.Domain.Repositories
{
    public class MongoContext
    {
        public IMongoCollection<User> Users => _database.GetCollection<User>("Users");

        public IMongoCollection<Initiative> Initiatives => _database.GetCollection<Initiative>("Initiatives");

        private readonly IMongoDatabase _database;

        public MongoContext(IConfiguration configuration)
        {
            var connectionString = configuration.GetSection("MongoConnection").Value;
            var mongoUrl = new MongoUrl(connectionString);
            var client = new MongoClient(connectionString);
            _database = client.GetDatabase(mongoUrl.DatabaseName);
        }

        public IMongoCollection<T> GetCollection<T>(string collectionName) where T : BaseEntity
        {
            return _database.GetCollection<T>(collectionName);
        }
    }
}
