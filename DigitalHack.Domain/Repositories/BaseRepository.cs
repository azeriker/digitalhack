﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using DigitalHack.Domain.Contracts;
using DigitalHack.Domain.Entities;
using MongoDB.Driver;

namespace DigitalHack.Domain.Repositories
{
    public abstract class BaseRepository<T> : IBaseRepository<T> where T : BaseEntity
    {
        protected IMongoCollection<T> Collection { get; }

        protected BaseRepository(MongoContext context, string collectionName)
        {
            Collection = context.GetCollection<T>(collectionName);
        }

        public void Add(T entity)
        {
            Collection.InsertOne(entity);
        }

        public T Get(string id)
        {
            return Collection.Find(i => i.Id == id).FirstOrDefault();
        }

        public IEnumerable<T> Get(IEnumerable<string> ids)
        {
            var filter = Builders<T>.Filter.In(i => i.Id, ids);
            return Collection.Find(filter).ToList();
        }

        public IEnumerable<T> Get()
        {
            return Collection.Find(_ => true).ToList();
        }

        public IEnumerable<T> Get<TField>(Expression<Func<T, TField>> field, TField value)
        {
            var filter = Builders<T>.Filter.Eq(field, value);
            return Collection.Find(filter).ToList();
        }
    }
}
