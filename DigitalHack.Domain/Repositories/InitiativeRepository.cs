﻿using DigitalHack.Domain.Contracts;
using DigitalHack.Domain.Entities;

using MongoDB.Driver;

namespace DigitalHack.Domain.Repositories
{
    public class InitiativeRepository : BaseRepository<Initiative>, IInitiativeRepository
    {
        public InitiativeRepository(MongoContext context)
            : base(context, $"{nameof(Initiative)}s")
        {
        }

        public void AddComment(string initiativeId, Comment comment)
        {
            var filter = Builders<Initiative>.Filter.Eq(i => i.Id, initiativeId);
            var update = Builders<Initiative>.Update
                .Push(i => i.CommentIds, comment.Id);

            Collection.UpdateOne(filter, update);
        }

        public void Update(Initiative initiative)
        {
            var filter = Builders<Initiative>.Filter.Eq(i => i.Id, initiative.Id);
            var update = Builders<Initiative>.Update
                .Set(i => i.CommentIds, initiative.CommentIds)
                .Set(i => i.Status, initiative.Status)
                .Set(i => i.Likes, initiative.Likes)
                .Set(i => i.Dislikes, initiative.Dislikes)
                .Set(i => i.ManagerComment, initiative.ManagerComment);

            Collection.UpdateOne(filter, update);
        }
    }
}
