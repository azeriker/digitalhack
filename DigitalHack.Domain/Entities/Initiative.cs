﻿using System;
using System.Collections.Generic;
using DigitalHack.Domain.BaseEntities;
using DigitalHack.Domain.Entities.Enums;
using MongoDB.Bson.Serialization.Attributes;

namespace DigitalHack.Domain.Entities
{
    public class Initiative : BaseEntity, IReactionable
    { 
        public Initiative()
        {
            Likes = new List<Like>();
            Dislikes = new List<Dislike>();
            CommentIds = new List<string>();
            Comments = new List<Comment>();
            Status = InitiativeStatus.New;
        }

        public string Title { get; set; }

        public string DistrictId { get; set; }

        public string Description { get; set; }

        public List<Like> Likes { get; set; }

        public List<Dislike> Dislikes { get; set; }

        public List<string> CommentIds { get; set; }

        public int UserId { get; set; }

        public DateTime Date { get; set; }

        public InitiativeStatus Status { get; set; }

        [BsonIgnore]
        public List<Comment> Comments { get; set; }

        public string ManagerComment { get; set; }

        [BsonIgnore] public int Rating => Likes.Count - Dislikes.Count;
    }
}
