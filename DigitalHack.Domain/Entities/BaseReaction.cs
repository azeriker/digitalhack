﻿namespace DigitalHack.Domain.Entities
{
    public abstract class BaseReaction
    {
        public int UserId { get; set; }
        
        public string EntityId { get; set; }
    }
}
