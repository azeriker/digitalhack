﻿namespace DigitalHack.Domain.Entities.Enums
{
    public enum EntityType
    {
        Initiative,
        Comment
    }
}
