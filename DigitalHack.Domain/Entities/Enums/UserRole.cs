﻿namespace DigitalHack.Domain.Entities.Enums
{
    public enum UserRole
    {
        Admin,
        Manager,
        User
    }
}
