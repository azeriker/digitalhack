﻿namespace DigitalHack.Domain.Entities.Enums
{
    public enum Sex
    {
        Male,
        Female
    }
}
