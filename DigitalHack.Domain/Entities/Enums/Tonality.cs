﻿namespace DigitalHack.Domain.Entities.Enums
{
    public enum Tonality
    {
        Unrecognized,
        Negative,
        Positive
    }
}
