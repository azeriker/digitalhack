﻿namespace DigitalHack.Domain.Entities.Enums
{
    public enum InitiativeStatus
    {
        New,
        InProgress,
        Done,
        Rejected
    }
}
