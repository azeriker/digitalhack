﻿namespace DigitalHack.Domain.Entities
{
    public class District : BaseEntity
    {
        public string Name { get; set; }
        public string ManagerId { get; set; }
    }
}
