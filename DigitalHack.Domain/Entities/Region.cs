﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DigitalHack.Domain.Entities
{
    public class Region : BaseEntity
    {
        public string Name { get; set; }

        public List<string> Districts { get; set; }
    }
}
