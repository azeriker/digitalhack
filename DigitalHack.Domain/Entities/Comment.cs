﻿using System;
using System.Collections.Generic;
using DigitalHack.Domain.BaseEntities;
using DigitalHack.Domain.Entities.Enums;
using MongoDB.Bson.Serialization.Attributes;

namespace DigitalHack.Domain.Entities
{
    public class Comment : BaseEntity, IReactionable
    {
        public Comment()
        {
            Likes = new List<Like>();
            Dislikes = new List<Dislike>();
            CommentIds = new List<string>();
            Comments = new List<Comment>();
            IsValidated = false;
            IsBanned = false;
            Tonality = Tonality.Unrecognized;
        }

        public string Message { get; set; }

        public Tonality Tonality { get; set; }

        public DateTime Date { get; set; }

        public List<Like> Likes { get; set; }

        public List<Dislike> Dislikes { get; set; }

        public List<string> CommentIds { get; set; }

        public int UserId { get; set; }

        public bool IsValidated { get; set; }

        public bool IsBanned { get; set; }

        [BsonIgnore]
        public List<Comment> Comments { get; set; }

        [BsonIgnore] public int Rating => Likes.Count - Dislikes.Count;
    }
}
