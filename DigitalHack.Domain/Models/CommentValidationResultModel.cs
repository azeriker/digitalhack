﻿using System.Collections.Generic;

namespace DigitalHack.Domain.Models
{
    public class CommentValidationResultModel
    {
        public List<string> BannedIds { get; set; }

        public List<string> ValidatedIds { get; set; }
    }
}
