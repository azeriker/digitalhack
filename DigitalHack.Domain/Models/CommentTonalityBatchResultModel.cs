﻿using System.Collections.Generic;

namespace DigitalHack.Domain.Models
{
    public class CommentTonalityBatchResultModel
    {
        public List<CommentTonalityResultModel> Result { get; set; }
    }
}
