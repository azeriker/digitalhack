﻿using DigitalHack.Domain.Entities.Enums;

namespace DigitalHack.Domain.Models
{
    public class CommentTonalityResultModel
    {
        public string Id { get; set; }

        public Tonality Tonality { get; set; }
    }
}
