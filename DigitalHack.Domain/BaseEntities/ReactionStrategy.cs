﻿using DigitalHack.Domain.Entities;

using System.Collections.Generic;
using System.Linq;

namespace DigitalHack.Domain.BaseEntities
{
    public interface IReactionable
    {
        List<Like> Likes { get; set; }

        List<Dislike> Dislikes { get; set; }
    }

    public class ReactionStrategy
    {
        public void Like(IReactionable reactionable, Like like)
        {
            if (CanReact(reactionable, like))
                reactionable.Likes.Add(like);
        }

        public void Dislike(IReactionable reactionable, Dislike dislike)
        {
            if (CanReact(reactionable, dislike))
                reactionable.Dislikes.Add(dislike);
        }

        public void UnLike(IReactionable reactionable, Like like)
        {
            reactionable.Likes = reactionable.Likes.Where(i => i.UserId != like.UserId).ToList();
        }

        public void UnDislike(IReactionable reactionable, Dislike dislike)
        {
            reactionable.Dislikes = reactionable.Dislikes.Where(i => i.UserId != dislike.UserId).ToList();
        }

        private bool CanReact(IReactionable reactionable, BaseReaction reaction)
        {
            return reactionable.Likes.Find(i => i.UserId == reaction.UserId) == null &&
                reactionable.Dislikes.Find(i => i.UserId == reaction.UserId) == null;
        }
    }
}
