﻿using DigitalHack.Domain.Entities;

using System.Collections.Generic;
using DigitalHack.Domain.Models;

namespace DigitalHack.Domain.Contracts
{
    public interface ICommentRepository : IBaseRepository<Comment>
    {
        void AddChild(string parentCommentId, Comment comment);

        IEnumerable<Comment> GetNotValidated();

        void MarkAsBanned(IEnumerable<string> ids);

        void MarkAsValidated(IEnumerable<string> ids);

        void UpdateTonality(CommentTonalityBatchResultModel result);

        void Update(Comment comment);
    }
}
