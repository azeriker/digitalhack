﻿using DigitalHack.Domain.Entities;

using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace DigitalHack.Domain.Contracts
{
    public interface IUserRepository
    {
        void Add(User user);

        User Get(int id);

        IEnumerable<User> Get();

        User Get<TField>(Expression<Func<User, TField>> field, TField value);

        bool Exists(string snils);

        void Update(User user);
    }
}
