﻿using System.Collections.Generic;
using DigitalHack.Domain.Entities;

namespace DigitalHack.Domain.Contracts
{
    public interface ICommentService
    {
        void Like(Like like);

        void Dislike(Dislike dislike);

        void UnLike(Like like);

        void UnDislike(Dislike dislike);

        void Ban(IEnumerable<string> ids);

        void Validate(IEnumerable<string> ids);

        void Add(Comment comment);

        void AddComment(string parentCommentId, Comment comment);

        Comment Get(string id);

        void Update(Comment comment);
    }
}
