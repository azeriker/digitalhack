﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using DigitalHack.Domain.Entities;

namespace DigitalHack.Domain.Contracts
{
    public interface IBaseRepository<T> where T : BaseEntity
    {
        void Add(T initiative);

        T Get(string id);

        IEnumerable<T> Get();

        IEnumerable<T> Get(IEnumerable<string> ids);

        IEnumerable<T> Get<TField>(Expression<Func<T, TField>> field, TField value);
    }
}
