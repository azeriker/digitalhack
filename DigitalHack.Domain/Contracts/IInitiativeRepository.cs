﻿using DigitalHack.Domain.Entities;

namespace DigitalHack.Domain.Contracts
{
    public interface IInitiativeRepository : IBaseRepository<Initiative>
    {
        void AddComment(string initiativeId, Comment comment);

        void Update(Initiative initiative);
    }
}
