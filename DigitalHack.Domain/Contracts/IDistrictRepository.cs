﻿using System.Collections.Generic;
using DigitalHack.Domain.Entities;

namespace DigitalHack.Domain.Contracts
{
    public interface IDistrictRepository : IBaseRepository<District>
    {
        IEnumerable<District> GetByManagerId(string managerId);
    }
}
