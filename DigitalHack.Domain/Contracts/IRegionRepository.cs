﻿using System;
using System.Collections.Generic;
using System.Text;
using DigitalHack.Domain.Entities;

namespace DigitalHack.Domain.Contracts
{
    public interface IRegionRepository
    {
        Region Get(string id);
        IEnumerable<Region> Get();
    }
}
