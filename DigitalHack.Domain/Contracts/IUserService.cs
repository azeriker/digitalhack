﻿using DigitalHack.Domain.Entities;

namespace DigitalHack.Domain.Contracts
{
    public interface IUserService
    {
        void Register(User user);

        User Get(int id);

        User GetBySnils(string snils);
    }
}
