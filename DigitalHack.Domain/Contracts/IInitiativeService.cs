﻿using System.Collections.Generic;
using DigitalHack.Domain.Entities;
using DigitalHack.Domain.Entities.Enums;

namespace DigitalHack.Domain.Contracts
{
    public interface IInitiativeService
    {
        void Like(Like like);

        void Dislike(Dislike dislike);

        void UnLike(Like like);

        void UnDislike(Dislike dislike);

        void AddComment(string initiativeId, Comment comment);

        string Add(Initiative initiative);

        IEnumerable<Initiative> Get();

        Initiative Get(string id);

        IEnumerable<Initiative> GetByDistrict(string districtId);

        void SetManagerComment(string initiativeId, string comment);

        void ChangeStatus(string initiativeId, InitiativeStatus status);
    }
}
