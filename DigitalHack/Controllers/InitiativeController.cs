﻿using DigitalHack.Domain.Contracts;
using DigitalHack.Domain.Entities;
using DigitalHack.Mappers;
using DigitalHack.ViewModels;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;
using System.Linq;

namespace DigitalHack.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize]
    public class InitiativeController : ControllerBase
    {
        private readonly IInitiativeService _initiativeService;
        private readonly InitiativeMapper _initiativeMapper;

        public InitiativeController(
            IInitiativeService initiativeService,
            InitiativeMapper initiativeMapper)
        {
            _initiativeService = initiativeService;
            _initiativeMapper = initiativeMapper;
        }

        [HttpPost]
        public void Like([FromBody] Like like)
        {
            like.UserId = int.Parse(User.Identity.Name);
            _initiativeService.Like(like);
        }

        [HttpPost]
        public void Dislike([FromBody] Dislike dislike)
        {
            dislike.UserId = int.Parse(User.Identity.Name);
            _initiativeService.Dislike(dislike);
        }

        [HttpPost]
        public void UnLike([FromBody] Like like)
        {
            like.UserId = int.Parse(User.Identity.Name);
            _initiativeService.UnLike(like);
        }

        [HttpPost]
        public void UnDislike([FromBody] Dislike dislike)
        {
            dislike.UserId = int.Parse(User.Identity.Name);
            _initiativeService.UnDislike(dislike);
        }

        [HttpPost]
        public void AddComment(string initiativeId, [FromBody] AddCommentViewModel model)
        {
            model.UserId = int.Parse(User.Identity.Name);
            var mapped = new Comment { Message = model.Message, UserId = model.UserId };
            _initiativeService.AddComment(initiativeId, mapped);
        }

        [HttpPost]
        public string Add([FromBody] AddInitiativeViewModel model)
        {
            model.UserId = int.Parse(User.Identity.Name);
            var mapped = _initiativeMapper.Map(model);
            return _initiativeService.Add(mapped);
        }

        [HttpGet]
        public IEnumerable<Initiative> Get()
        {
            return _initiativeService.Get();
        }

        [HttpGet]
        public Initiative GetById(string id)
        {
            return _initiativeService.Get(id);
        }

        [HttpGet]
        public IEnumerable<Initiative> GetByDistrict(string districtId)
        {
            return _initiativeService.GetByDistrict(districtId).ToList();
        }
    }
}
