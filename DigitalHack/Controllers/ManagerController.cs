﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DigitalHack.Domain.Contracts;
using DigitalHack.Domain.Entities.Enums;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DigitalHack.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize]
    public class ManagerController : ControllerBase
    {
        private readonly IInitiativeService _service;

        public ManagerController(IInitiativeService service)
        {
            _service = service;
        }

        [HttpPost]
        public void ChangeStatus(string initiativeId, string status)
        {
            _service.ChangeStatus(initiativeId, Enum.Parse<InitiativeStatus>(status));
        }

        [HttpPost]
        public void ChangeInitiativeManagerComment(string initiativeId, string comment)
        {
            _service.SetManagerComment(initiativeId, comment);
        }
    }
}