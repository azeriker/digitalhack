﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DigitalHack.Domain.Contracts;
using DigitalHack.Domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DigitalHack.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class RegionController
    {
        private readonly IRegionRepository _regionRepository;
        public RegionController(IRegionRepository regionRepository)
        {
            _regionRepository = regionRepository;
        }

        [HttpGet]
        public IEnumerable<Region> Get()
        {
            return _regionRepository.Get();
        }
    }
}
