﻿using DigitalHack.Domain.Contracts;
using DigitalHack.Domain.Entities;
using DigitalHack.Domain.Models;
using DigitalHack.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace DigitalHack.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize]
    public class CommentController : ControllerBase
    {
        private readonly ICommentRepository _commentRepository;
        private readonly ICommentService _commentService;

        public CommentController(
            ICommentRepository commentRepository,
            ICommentService commentService)
        {
            _commentRepository = commentRepository;
            _commentService = commentService;
        }

        [HttpPost]
        public void Like([FromBody] Like like)
        {
            like.UserId = int.Parse(User.Identity.Name);
            _commentService.Like(like);
        }

        [HttpPost]
        public void Dislike([FromBody] Dislike dislike)
        {
            dislike.UserId = int.Parse(User.Identity.Name);
            _commentService.Dislike(dislike);
        }

        [HttpPost]
        public void UnLike([FromBody] Like like)
        {
            like.UserId = int.Parse(User.Identity.Name);
            _commentService.UnLike(like);
        }

        [HttpPost]
        public void UnDislike([FromBody] Dislike dislike)
        {
            dislike.UserId = int.Parse(User.Identity.Name);
            _commentService.UnDislike(dislike);
        }

        [HttpPost]
        public void SendValidationResult([FromBody] CommentValidationResultModel model)
        {
            _commentService.Ban(model.BannedIds);
            _commentService.Validate(model.ValidatedIds);
        }

        [HttpPost]
        public void SendTonalityResult([FromBody] CommentTonalityBatchResultModel model)
        {
            _commentRepository.UpdateTonality(model);
        }

        [HttpPost]
        public void AddComment(string parentCommentId, [FromBody] AddCommentViewModel model)
        {
            model.UserId = int.Parse(User.Identity.Name);
            var mapped = new Comment { Message = model.Message, UserId = model.UserId };
            _commentService.AddComment(parentCommentId, mapped);
        }

        //[HttpGet]
        //public Comment GetById(string id)
        //{
        //    return _commentService.Get(id);
        //}

        [HttpGet]
        public IEnumerable<Comment> GetNotValidated()
        {
            return _commentRepository.GetNotValidated();
        }
    }
}