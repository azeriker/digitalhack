﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using System.Net;
using DigitalHack.Domain.Contracts;
using DigitalHack.Domain.Entities;
using DigitalHack.Domain.Entities.Enums;
using DigitalHack.ViewModels;

namespace DigitalHack.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IUserService _service;

        public AccountController(IUserService service)
        {
            _service = service;
        }

        [HttpPost("register")]
        public HttpStatusCode Register([FromBody]RegisterViewModel model)
        {
            _service.Register(new User
            {
                Email = model.Email,
                Id = model.Id,
                Phone = model.Phone,
                Snils = model.Snils,
                Age = model.Age,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Rating = model.Rating,
                Role = model.Role,
                Sex = model.Sex
            });

            return HttpStatusCode.Accepted;
        }

        [HttpGet]
        public string GetRole()
        {
            if(int.TryParse(User.Identity.Name, out var userId))
                return _service.Get(userId).Role.ToString();
            return "";

        }

        [Authorize]
        [HttpGet("test")]
        public void Test()
        {
            var user = User.Identity.Name;
        }
    }
}