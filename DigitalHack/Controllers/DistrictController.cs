﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DigitalHack.Domain.Contracts;
using DigitalHack.Domain.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DigitalHack.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class DistrictController : ControllerBase
    {
        private readonly IDistrictRepository _districtRepository;
        private readonly IRegionRepository _regionRepository;

        public DistrictController(IDistrictRepository districtRepository, IRegionRepository regionRepository)
        {
            _districtRepository = districtRepository;
            _regionRepository = regionRepository;
        }

        [HttpGet]
        public IEnumerable<District> GetByRegion(string regionId)
        {
            var region = _regionRepository.Get(regionId);
            var districts = _districtRepository.Get(region.Districts);
            return districts;
        }

        [HttpGet]
        public IEnumerable<District> GetByManagerId(string managerId)
        {
            return _districtRepository.GetByManagerId(managerId);
        }
    }
}