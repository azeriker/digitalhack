﻿using DigitalHack.Domain.Entities;
using DigitalHack.ViewModels;

namespace DigitalHack.Mappers
{
    public class InitiativeMapper
    {
        public Initiative Map(AddInitiativeViewModel model)
        {
            return new Initiative
            {
                Title = model.Title,
                Description = model.Description,
                UserId = model.UserId,
                DistrictId = model.DistrictId
            };
        }
    }
}
