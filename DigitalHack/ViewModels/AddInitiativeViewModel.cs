﻿namespace DigitalHack.ViewModels
{
    public class AddInitiativeViewModel
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public int UserId { get; set; }

        public string DistrictId { get; set; }
    }
}
