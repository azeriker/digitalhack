﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigitalHack.ViewModels
{
    public class PublishInitiativeViewModel
    {
        public string Title { get; set; }

        public string Text { get; set; }

        public string DistrictId { get; set; }

    }
}
