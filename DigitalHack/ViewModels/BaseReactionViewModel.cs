﻿namespace DigitalHack.ViewModels
{
    public class BaseReactionViewModel
    {
        public int UserId { get; set; }

        public string EntityId { get; set; }
    }
}
