﻿using DigitalHack.Domain.Entities.Enums;

namespace DigitalHack.ViewModels
{
    public class RegisterViewModel
    {
        public int Id { get; set; }

        public UserRole Role { get; set; }

        public string Email { get; set; }

        public string Snils { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Phone { get; set; }

        public int Rating { get; set; }

        public int Age { get; set; }

        public Sex Sex { get; set; }
    }
}
