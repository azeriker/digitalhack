﻿namespace DigitalHack.ViewModels
{
    public class AddCommentViewModel
    {
        public string Message { get; set; }

        public int UserId { get; set; }
    }
}
